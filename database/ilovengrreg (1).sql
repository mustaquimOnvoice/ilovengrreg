-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2019 at 08:21 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilovengrreg`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) NOT NULL,
  `image_url` text NOT NULL,
  `type` int(1) NOT NULL COMMENT '0=profile',
  `ref_code` varchar(20) NOT NULL COMMENT 'atsm_code',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scheme_date` varchar(25) DEFAULT NULL,
  `deleted_on` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reg_user`
--

CREATE TABLE `reg_user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `number` bigint(20) NOT NULL,
  `ward` int(11) NOT NULL,
  `address` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reg_user`
--

INSERT INTO `reg_user` (`id`, `name`, `number`, `ward`, `address`, `created_at`) VALUES
(1, 'demo', 9762379774, 4, '606 Killi , akole', '2019-08-05 07:36:24'),
(2, 'theme3', 9175917762, 5, '606 Killi , akole', '2019-08-06 07:42:12'),
(3, 'anuronERP', 9175917762, 2, '606 Killi , akole', '2019-08-05 07:44:18'),
(4, 'Hinf', 9175917762, 2, '78, babui chowk', '2019-08-06 07:46:33'),
(5, 'Testing', 9175917762, 15, '716, safjkfjk, jklsadjkjk, ahmedl', '2019-08-05 07:57:12'),
(6, 'Sana Sayyed', 9823658560, 17, '606 vimangar, pune', '2019-08-06 08:21:47'),
(7, 'anuronERP', 8087586743, 6, '606 vimangar, pune', '2019-08-06 09:16:32'),
(8, 'theme33', 9175917762, 5, '78, babui chowk', '2019-08-06 09:17:18'),
(9, 'theme33', 9175917762, 4, '608 station road,', '2019-08-05 09:18:49'),
(10, 'theme3', 9175917762, 6, '78, babui chowk', '2019-08-06 09:32:30'),
(11, 'Temp1', 9175917762, 5, '608 station road,', '2019-08-05 09:43:32');

-- --------------------------------------------------------

--
-- Table structure for table `sms_sent`
--

CREATE TABLE `sms_sent` (
  `id` int(11) NOT NULL,
  `number` text NOT NULL,
  `message` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_sent`
--

INSERT INTO `sms_sent` (`id`, `number`, `message`, `created_at`) VALUES
(1, '9175917762,8087586743', 'This is subject, The message details here asdsada', '2019-08-08 09:40:14'),
(2, '9762379774,9175917762,9823658560,8087586743', 'This is subject, The message details here asdsada', '2019-08-08 12:14:39');

-- --------------------------------------------------------

--
-- Table structure for table `wwc_admin`
--

CREATE TABLE `wwc_admin` (
  `id` int(11) NOT NULL,
  `fname` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1=SuperAdmin,2=Admin',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `ip_address` text,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL,
  `recent_login` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wwc_admin`
--

INSERT INTO `wwc_admin` (`id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `contact`, `profile_pic`, `type`, `status`, `ip_address`, `inserted_on`, `updated_on`, `deleted_on`, `recent_login`) VALUES
(1, 'Prohance', '', 'Mom', 'sadmin', 'e10adc3949ba59abbe56e057f20f883e', 'sadmin@gmail.com', '9175917762', 'letsup.png', 1, 1, NULL, '2018-11-26 00:00:00', '2019-01-02 12:19:52', '2019-04-29 11:25:22', NULL),
(2, 'Prohance', '', 'Mom', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin@gmail.com', '9175917762', 'letsup.png', 1, 1, NULL, '2018-11-26 00:00:00', '2019-01-02 12:19:52', '2019-04-29 11:25:22', NULL),
(3, 'Prohance', '', 'Mom', 'client', 'e10adc3949ba59abbe56e057f20f883e', 'client@gmail.com', '9175917762', 'letsup.png', 1, 1, NULL, '2018-11-26 00:00:00', '2019-01-02 12:19:52', '2019-04-29 11:25:22', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reg_user`
--
ALTER TABLE `reg_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_sent`
--
ALTER TABLE `sms_sent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wwc_admin`
--
ALTER TABLE `wwc_admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reg_user`
--
ALTER TABLE `reg_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sms_sent`
--
ALTER TABLE `sms_sent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wwc_admin`
--
ALTER TABLE `wwc_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
