<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	public function dashboard()
	{		
		$select	 = array('*');
		$where = array();
		$mob ='';
		if($this->session->userdata('mob')){
			$this->session->userdata('mob');
		}
	
	  //Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Admin/dashboard";
		$config["total_rows"] = $this->base_models->get_count('id','reg_user', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination('reg_user', $where,'id',$config["per_page"], $page);     
		//Pagination End

		$mob = (@$mob) ? $mob : '';
		$pagedata['select']=array('mob'=>$mob);
		$this->renderView('Admin/register_list',$pagedata);
	}
	
	public function register_list_sess()
	{
		$select	 = array('*');
		$where = array();
		$mob ='';
		//Filter Process	
		if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls')
		{
			/*$ranges = explode('-',$this->input->post('daterange'));
			$fdate = date('Y-m-d', strtotime($ranges[0])).' 00:00:00';
			$todate = date('Y-m-d', strtotime($ranges[1])).' 23:59:00';*/
			$fdate = (@$this->input->post('fromdate')) ? $this->input->post('fromdate').' 00:00:00' : '';
			$todate = (@$this->input->post('todate')) ? $this->input->post('todate').' 23:59:00' : '';
			$mob_no = (@$this->input->post('mob')) ? $this->input->post('mob') : '';
			$mob = trim($mob_no);
			$where = array();
			if($this->input->post('fromdate') != '' && $this->input->post('todate') != ''){
				if($this->session->userdata('fromdate') != NULL){
					$fdate = (@$this->session->userdata('fromdate')) ? $this->session->userdata('fromdate') : '';
					$todate = (@$this->session->userdata('todate')) ? $this->session->userdata('todate') : '';
					$where = "created_at BETWEEN '$fdate' AND '$todate'";
				}else{
					$where = "created_at BETWEEN '$fdate' AND '$todate'";
					$this->session->set_userdata(array("fromdate"=>$fdate,"todate"=>$todate));
				}
			}

			$array_items = $this->session->set_userdata(array("mob"=>$mob));
			if($this->session->userdata('mob') != NULL){
				$mob = $this->session->userdata('mob'); 
				$filter =  array('number'=> $mob);
				$where = array_merge($where,$filter);
			} elseif ($mob !=''){
				$filter =  array('number'=> $mob);
				$where = @array_merge($where,$filter);	
			}
		 
		}else{
			if($this->session->userdata('fromdate') != NULL){
				$fdate = (@$this->session->userdata('fromdate')) ? $this->session->userdata('fromdate') : '';
				$todate = (@$this->session->userdata('todate')) ? $this->session->userdata('todate') : '';
				$where = "created_at BETWEEN '$fdate' AND '$todate'";
				
			}
			if($this->session->userdata('mob') != NULL){
				$mob = $this->session->userdata('mob'); 
				$filter =  array('number'=> $mob);
				$where .= "AND number = '$mob'";
			}
		}
		
		if(@$_POST['submit']=='createxls') {
			$data['data'] = $this->base_models->GetAllValues('reg_user', $where, $select, true);
			$this->generate_reg_excel($data['data']);
		}
		//End Filter Process
	
	  //Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Admin/register_list_sess";
		$config["total_rows"] = $this->base_models->get_count('id','reg_user', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination('reg_user', $where,'id',$config["per_page"], $page);     
		//Pagination End

		$mob = (@$mob) ? $mob : '';
		$pagedata['select']=array('mob'=>$mob);  
		$this->renderView('Admin/register_list',$pagedata);
	}
	
	//generate to excel	
	public function generate_reg_excel($param1){
		// create file name
		$fileName = 'RegisterList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Mob No.');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Ward No.');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Address');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Date');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['number']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['ward']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['address']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, date('d-M-y', strtotime($element['created_at'])));
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
	public function send_sms()
	{		
		$select = array('id','name','number');
		$where = array();
		$pagedata['data'] = $this->base_models->GetAllValues('reg_user', $where, $select, true);
		$this->renderView('Admin/Sms/send_sms',$pagedata);
	}
	
	public function push_sms()
	{		
		if($_POST['users'][0] == 0) { // if selected all then fetch all users for db
			$select = array('number');			
			$nums = array_values(array_unique(array_column($this->base_models->get_records('reg_user',$select,FALSE),'number')));
		}else{
			$nums = array_values(array_unique($_POST['users']));			
		}
		
		$subj = trim($this->input->post('subject'));
		$message = trim($this->input->post('message'));
		$msg = "$subj, $message";
		$this->message_send_bulk($msg, $nums);
		
		$numsStr = implode(',',$nums);
		$insert_array=array(
						'number'=>$numsStr,
						'message'=>$msg
						);
		if($this->base_models->add_records('sms_sent',$insert_array)){
			$this->session->set_flashdata('success','SMS Sent successfully');
		}else{
			$this->session->set_flashdata('error','SMS Not Send');
		}
		redirect(base_url('Admin/send_sms'));
	}
	
	public function send_email()
	{		
		echo $this->email_send();
		die();
	}
	public function mail()
	{

				$name 	= 'Ilovengr';
				$email 	= 'rs02419090@gmail.com';
				$message= 'Demo';
				$date   = @date('d-m-Y');

				$to = "msayyed.it@gmail.com";
				$subject = "Enquiry From $name";
				$message = "Demo email";
				$header = "From:$email \r\n";
				//$header .= "cc:holispiritcbse@gmail.com \r\n";
				$header .= "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html \r\n";				
				$retval = mail($to,$subject,$message,$header);	
				if($retval == true)
				{
					echo 'Email sent.';
				}
				else
				{
					show_error($this->email->print_debugger());
				}
		
	}
	//END Anuron ERP--//
	
	// public function dashboard()
	// {		
		// $pagedata['total_users'] = $this->base_models->count_users();
		// $pagedata['total_clients'] = $this->base_models->count_clients();
		// $pagedata['total_pending_adv'] = $this->base_models->count_adv('0');
		// $pagedata['total_rejected_adv'] = $this->base_models->count_adv('1');
		// $pagedata['total_approved_adv'] = $this->base_models->count_adv('2');
		// $pagedata['total_publish_adv'] = $this->base_models->count_adv('3');
		// $pagedata['total_pending_invoice'] = $this->base_models->count_invoice('2');
		// $pagedata['total_approved_invoice'] = $this->base_models->count_invoice('1');
		
		// $this->renderView('Admin/dashboard',$pagedata);
	// }
	
	//---------- Business cat ------------//
	public function business_cat()
	{
		$status_str = 'Business Category';
		$where_array = array('status =' => '1');
		$data['data'] = $this->base_models->get_records('business_cat',array('id','bname'),$where_array);
		$pagedata = array('data'=>$data['data'],'delete_link'=>'Admin/delete_business_cat', 'title' => $status_str);
		$this->renderView('Admin/business_cat',$pagedata);
	}
	
	public function add_business(){
		$this->form_validation->set_rules('bname', 'Business name', 'trim|required');
		if($this->form_validation->run())
		{
			$insert_array=array(
						'bname'=>$this->input->post('bname'),
						'user_id'=>$this->session->userdata('user_type'),
						'added_on'=>date("Y-m-d H:i:s")
						);
			if($this->base_models->add_records('business_cat',$insert_array)){
				$this->session->set_flashdata('success','Added successfully');
			}else{
				$this->session->set_flashdata('error','Not added Please try again');
			}
		}
		redirect(base_url('Admin/business_cat'));
	}
	
	public function edit_business_cat()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->GetSingleDetails('business_cat', array('id' => $id), array('id','bname'));
		$this->renderView('Admin/edit-business-cat',$pagedata);
	}
	
	public function update_business_cat()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('admin/business_cat')); 
		}		
		$this->form_validation->set_rules('bname', 'Business name', 'trim|required');	
		$error='';
			if($this->form_validation->run())
			{				
				$update_array=array(
						'bname'=>$this->input->post('bname')
					);
				$where_array = array('id'=>$id);
				if($this->base_models->update_records('business_cat',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not Edited Please try again');
				}
			}
				redirect(site_url('admin/edit_business_cat/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
	
	public function delete_business_cat()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2'
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('business_cat',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- End Business cat--//
	
	//---------- User ------------//
	public function users()
	{		
		$pagedata['results'] = $this->base_models->get_users();
		$pagedata['delete_link'] = 'Admin/delete_user';
		$this->renderView('Admin/users',$pagedata);
	}
	
	public function add_user()
	{
		$this->renderView('Admin/add-user');
	}
	
	public function insert_user()
	{
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            if(!empty($_FILES['image']['name'])){
                $config['upload_path'] = 'uploads/admin/users/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->upload->initialize($config);
				if($this->upload->do_upload('image')){
					$data = $this->upload->data();
				}else{
                    $imageerrors = $this->upload->display_errors();
					$this->form_validation->set_message('image', $imageerrors);					
                }
			}
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>md5($this->input->post('inputPassword')),
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						'profile_pic'=>$data['file_name'],
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('wwc_admin',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(base_url('admin/users'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
				$this->renderView('Admin/add-user');
	}
	
	public function edit_user()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->get_users('',$id);
		$this->renderView('Admin/edit-user',$pagedata);
	}
	
	public function update_user()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(base_url('admin/users')); 
		}
		
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('inputPassword') != ''){
				$password = md5($this->input->post('inputPassword'));
			}else{
				$password = $this->input->post('oldpassword');
			}	
			if($this->form_validation->run())
			{				
				$update_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>$password,
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(base_url('admin/edit_user/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_user()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- End User ------------//
	
public function add_file()
	{
		$pagedata['category_details']=$this->db->get('tbl_category')->result_array();
		$this->load->view('Admin/add-file',$pagedata);
	}
	
public function upload_file()
	{
		if(isset($_POST['submit']))
		{
			
			$file=$_FILES['file']['name'];
			$category_id=$_POST['category_id'];
			$upload_date=@date('d-m-Y');
			$status=0;
			
			$file=$file.time();
			$string = str_replace(" ","-", $file);
			// echo $string;
			// die();
			$data['file_name']=$string;
			$data['category_id']=$category_id;
			$data['upload_date']=$upload_date;
			$data['status']=$status;
			
			move_uploaded_file($_FILES['file']['tmp_name'],'uploads/'.$string.'.zip');
			$this->db->insert('tbl_uploads',$data);
			$this->session->set_flashdata('success', 'File Upload Successfully..!!');
			redirect('Admin/add_file/');
		}
	}

public function view_file()
	{
		$pagedata['file_details']=$this->db->query('select * from tbl_uploads as tu JOIN tbl_category as tc on tu.category_id=tc.id order by tu.file_id desc')->result_array();
		$this->load->view('Admin/view-file',$pagedata);
	}	

public function edit_file($file_id)
	{
		$pagedata['file_details']=$this->db->query("select * from tbl_uploads as tu JOIN tbl_category as tc on tu.category_id=tc.id where tu.file_id='$file_id'")->result_array();
		$pagedata['category_details']=$this->db->get('tbl_category')->result_array();
		$this->load->view('Admin/edit-file',$pagedata);
	}	

public function update_file()
	{
		if(isset($_POST['submit']))
		{
			
			$file_id=$_POST['file_id'];
			$old_file=$_POST['old_file'];
			$category_id=$_POST['category_id'];

			if($_FILES['file']['name']!='')
				{
					$file=$_FILES['file']['name'];
					$file=$file.time();
					$string = str_replace(" ","-", $file);
					move_uploaded_file($_FILES['file']['tmp_name'],'uploads/'.$string.'.zip');
					unlink($base_url.'uploads/'.$old_file.'.zip');
				}
			else
				{
					$file=$old_file;
				}
			
			if($_POST['status']!='')
				{
					$status=$_POST['status'];
				}
			else
				{
					$status=$_POST['old_status'];
				}
				
			$data['file_name']=$file;
			$data['category_id']=$category_id;
			$data['status']=$status;
			
			$this->db->where('file_id',$file_id);
			$this->db->update('tbl_uploads',$data);
			$this->session->set_flashdata('success', 'File Update Successfully..!!');
			redirect('Admin/view_file/');
		}
	}
	
public function delete_file($old_file,$file_id)
	{
		
		$this->db->where('file_id',$file_id);
		$this->db->delete('tbl_uploads');
		unlink($base_url.'uploads/'.$old_file.'.zip');
		$this->session->set_flashdata('delete', 'File Delete Successfully..!!');
		redirect('Admin/view_file/');
	}

// public function profile()
	// {
		// $user_id=$this->session->userdata('user_id');
		// $this->db->where('user_id',$user_id);
		// $pagedata['profile_details']=$this->db->get('tbl_users')->result_array();
		// $this->load->view('Admin/profile',$pagedata);
	// }
	
// public function edit_profile()
	// {
		// if(isset($_POST['submit']))
		  // {
			// $user_id=$_POST['user_id'];
			// $name=$_POST['name'];
			// $mno=$_POST['mno'];
			// $email=$_POST['email'];
			// $address=$_POST['address'];
			
			// $data['name']=$name;
			// $data['mno']=$mno;
			// $data['email']=$email;
			// $data['address']=$address;
		
			// $this->db->where('user_id',$user_id);
			// $this->db->update('tbl_users',$data);
			// $this->session->set_flashdata('success', 'Profile Update Successfully..!!');
			// redirect('Admin/profile/');
		  // }
			
	// }

// public function change_password()
	// {
		// if(isset($_POST['submit']))
		  // {
				// //User Id Take From Sesssion
				// $users_id=$_POST['users_id'];
				// $old_password=$_POST['old_password'];
				// $password1=$_POST['old-password'];
				
				// $new_password=$_POST['new_password'];
				// $confirm_password=$_POST['confirm_password'];
				
				// $data['password']=$new_password;
				
				// $this->db->where('user_id',$users_id);
				// $this->db->update('tbl_users',$data);
				// $this->session->set_flashdata('success', 'Password Change Successfully!!!');
				// redirect('Admin/profile/');

				
		  // }
	// }

	 	
		
}
