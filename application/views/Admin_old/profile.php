    <title>Profile</title>
    <!-- Bootstrap Core CSS -->
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url();?>assets/css/colors/default.css" id="theme" rel="stylesheet">
	<!-- color CSS -->
    <link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme" rel="stylesheet">
    
   
</head>

<body class="fix-sidebar">
    
    <div id="wrapper">
        <!-- Top Navigation -->
         <?php echo $header;?>
       <?php echo $nav;?>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Profile</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo site_url($this->session->userdata('login_type').'/dashboard/');?>">Dashboard</a></li>
							<li class="active"><a href="<?php echo site_url($this->session->userdata('login_type').'/profile/');?>">View Profile</a></li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <ul class="nav customtab nav-tabs" role="tablist">
                                <li role="presentation" class="nav-item"><a href="#profile" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> My Profile</span></a></li>
                                <li role="presentation" class="nav-item"><a href="#password" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Change Password</span></a></li>
                                
                            </ul>
                            <div class="tab-content">
								<h4 class="box-title m-b-0 text-center" style="color:#03a9f3;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('success');?> </h4>
                                <div class="tab-pane active" id="profile">
									<form class="form-horizontal form-material" action="<?php echo site_url($this->session->userdata('login_type').'/edit_profile');?>" method="POST" data-toggle="validator" >
										<div class="row">
										  <div class="form-group col-sm-4">
											<label for="name" class="control-label bold">First Name</label>
											<input type="text" class="form-control" id="fname" name="fname" placeholder="Enter First Name" value="<?php echo $profile_details->fname;?>" data-error="First Name is required" required>
											<div class="help-block with-errors"></div>
										  </div>
										  <div class="form-group col-sm-4">
											<label for="name" class="control-label">Middle Name</label>
											<input type="text" class="form-control" id="mname" name="mname" placeholder="Enter Middle Name" value="<?php echo $profile_details->mname;?>" data-error="Middle Name is required" required>
											<div class="help-block with-errors"></div>
										  </div>
										  <div class="form-group col-sm-4">
											<label for="name" class="control-label">Last Name</label>
											<input type="text" class="form-control" id="lname" name="lname" placeholder="Enter Last Name" value="<?php echo $profile_details->lname;?>" data-error="Last Name is required" required>
											<div class="help-block with-errors"></div>
										  </div>
									  </div>
									<div class="row">
									  <div class="form-group col-sm-4">
										<label for="name" class="control-label">User Name</label>
										<input type="text" class="form-control" id="username" name="username" placeholder="Enter User Name" value="<?php echo $profile_details->username;?>" data-error="User Name is required" required>
										<div class="help-block with-errors"></div>
									  </div>
									  <div class="form-group col-sm-4">
										<label for="email" class="control-label">Email</label>
										<input type="email" class="form-control" id="email" name="email" value="<?php echo $profile_details->email;?>" placeholder="Enter Email">
										<div class="help-block with-errors"></div>
									  </div>
									</div>
										<div class="form-group">
											<div class="col-sm-12">
												<button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Update Profile</button>
												<input type="hidden" name="id" value="<?php echo $profile_details->id;?>" class="form-control form-control-line">
											</div>
										</div>										
									</form>
                                </div>
                                <div class="tab-pane" id="password">
                                    <form data-toggle="validator" class="form-horizontal form-material" action="<?php echo site_url($this->session->userdata('login_type').'/change_password');?>" method="POST">
										<div class="form-group">
											<div class="col-md-12">
												<label for="inputPassword" class="control-label">Password</label>
												<div class="row">
												  <div class="form-group col-sm-6">
													<input type="password" data-toggle="validator" data-minlength="6" name="inputPassword" class="form-control" id="inputPassword" placeholder="Password" required>
													<span class="help-block">Minimum of 6 characters</span> </div>
												  <div class="form-group col-sm-6">
													<input type="password" class="form-control" name="inputPasswordConfirm" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Confirm Password" required>
													<div class="help-block with-errors"></div>
												  </div>
												</div>
											</div>
										</div>
										<div class="form-group">
                                            <div class="col-md-12">
                                                <button id="button" type="submit" name="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Change Password</button>
												<input type="hidden" name="id" value="<?php echo $profile_details->id;?>" class="form-control form-control-line">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
           <?php echo $footer;?>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url();?>assets/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/validator.js"></script>
    <!--Style Switcher -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
<script>
setTimeout(function() {
	$('#timeout').fadeToggle('slow');
}, 3000);	
</script>
