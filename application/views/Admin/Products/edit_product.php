<title>Edit</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
<?php //die('s');?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Edit</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <!-- .row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <form method="post" name="edit-form" id="edit-form" action="<?php echo site_url(); ?>/Products/update_product?id=<?php echo base64_encode($data->id);?>" enctype="multipart/form-data" data-toggle="validator" >
				  <div class="row">
					  <div class="form-group col-sm-3">
						<label for="model" class="control-label">Model</label>
						<textarea rows="3" name="model" id="model" class="form-control" ><?php echo $data->model;?></textarea>
						<div class="help-block with-errors"></div>
					  </div>
					  <div class="form-group col-sm-4">
						<label for="tech_spec" class="control-label">Technical Specification</label>
						<textarea rows="3" name="tech_spec" id="tech_spec" class="form-control" ><?php echo $data->tech_spec;?></textarea>
						<div class="help-block with-errors"></div>
					  </div>
					  <div class="form-group col-sm-5">
						<label for="usps" class="control-label">USPS</label>
						<textarea rows="3" name="usps" id="usps" class="form-control" ><?php echo $data->usps;?></textarea>
						<div class="help-block with-errors"><?php if(form_error('usps')!=""){ echo form_error('usps');} ?><span class="text-danger" id="ErrorContent"></span></div>
					  </div>
				  </div>
				  <div class="row">
					  <div class="form-group col-sm-3">
						<label for="dp" class="control-label">DP</label>
						<input type="number" class="form-control" id="dp" name="dp" value="<?php echo $data->dp;?>" placeholder="DP" data-error="DP required" required>
						<div class="help-block with-errors"></div>
						<div id="exist_error"></div>
					  </div>
					  <div class="form-group col-sm-3">
						<label for="mop" class="control-label">MOP</label>
						<input type="number" class="form-control" id="mop" name="mop" value="<?php echo $data->mop;?>" placeholder="MOP" data-error="MOP required" required>
						<div class="help-block with-errors"></div>
						<div id="exist_error"></div>
					  </div>
					  <div class="form-group col-sm-3">
						<label for="mrp" class="control-label">MRP</label>
						<input type="number" class="form-control" id="mrp" name="mrp" value="<?php echo $data->mrp;?>" placeholder="MRP" data-error="MRP required" required>
						<div class="help-block with-errors"></div>
						<div id="exist_error"></div>
					  </div>
					  <div class="form-group col-sm-3">
						<label for="image" class="control-label">Image</label>
						<input type="hidden" name="image1" id="image1" value="<?php echo $data->image; ?>"/>
						<input type="file" class="form-control" id="image" name="image" value="<?php echo set_value('image');?>">
						<div class="help-block with-errors"><?php if(form_error('image')!=""){ echo form_error('image');} ?></div>
					  </div>
				  </div>
				  <div class="row">
					<div class="form-group col-sm-3">
						<label for="prod_status" class="control-label">Status</label>
						<select class="form-control" id="prod_status" name="prod_status" data-error="Status required" required>
							<option <?php echo ($data->prod_status == '1') ? 'selected' : ''; ?> value="1">Active</option>
							<option <?php echo ($data->prod_status == '0') ? 'selected' : ''; ?> value="0">Inactive</option>
						</select>
					</div>
				  </div>
				 <div class="form-group">
					<div class="row">
					  <div class="form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
					  </div>
					</div>
				 </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.row -->      
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/admin/item.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
