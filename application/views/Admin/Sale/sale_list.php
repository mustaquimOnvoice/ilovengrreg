<title>Sale list</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
<script>
	var tblexport = true;
	console.log('tblexport: '+tblexport);
  console.log('CSS: assets/css/common/listing.css');
</script>
<!-- Listing CSS -->
<link href="<?php echo base_url();?>assets/css/common/listing.css"   rel="stylesheet">
</head>

<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Sale List</h4>
        </div>
        <!--<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Table</a></li>
            <li class="active">Data Table</li>
          </ol>
        </div>-->
        <!-- /.col-lg-12 -->
      </div>
      <!-- /row -->

  <div class="row">
    <div class="white-box col-sm-12">
      <form method="post" name="date-form" id="date-form" action="<?php echo site_url('Adminsales/sale_list_sess'); ?>" data-toggle="validator">       
        <div class="col-sm-2">
            <input type="text" class="form-control" id="imei" name="imei" placeholder="IMEI" value="<?php @$imei = $select['imei']; echo ($imei == '') ? '' : $imei ; ?>">
        </div>
        <div class="col-sm-2">
        <select class="form-control select2" id="nd_id" name="nd_id" >
            <option <?php echo ($select['nd_id'] == null ) ? 'selected' : ''; ?> value="" >select</option>
            <?php foreach($nd_list as $row){ ?>
            <option <?php echo ($row['nd_id'] == $select['nd_id']) ? 'selected' : ''; ?> value="<?php echo $row['nd_id'];?>"><?php echo $row['nd_code'];?></option>
           <?php } ?>
       </select>
              </div>
        <div class="col-sm-2">
        <select class="form-control select2" id="status" name="status" >
            <option <?php echo ($select['status'] == null ) ? 'selected' : ''; ?> value="" >select</option>
            <option <?php echo ($select['status'] == '0') ? 'selected' : ''; ?> value="0">Unsold</option>
            <option <?php echo ($select['status'] == '1') ? 'selected' : ''; ?> value="1">Sold</option>
       </select>
              </div>
           
        <div class="col-sm-2">
          <button type="submit" name="submit" value="filter" class="pull-left btn-xs btn-success">Search</button>       
          <button type="submit" name="submit" value="createxls" class="pull-left btn-xs btn-primary"><i class="fa fa-file-excel-o"></i> Export Excel</button>
        </div>
      </form>
     
    </div>
  </div>      
		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <div class="row">
		<div class="col-sm-12">
          <div class="white-box">
            <div class="table-responsive">
                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>IMEI</th>
                            <th>Item Code</th>
                            <th>ND Code</th>
                            <th>ND Firm Name</th>
                            <th>Status</th>
                            <th>Upload Date</th>
                         	<th>Action</th>
                        </tr>
                    </thead>
                 
                    <tbody>
                        <?php   $i = 1;
							if(count($results)>0){
								foreach($results as $row){ ?>
									<tr>
										<td><?php echo $i;?></td>
										<td><?php echo $row['imei'];?></td>
										<td><?php echo $row['item_code'];?></td>
                    <td><?php echo $row['nd_code'];?></td>
                    <td><?php $nd_code = $row['nd_code'];
                    $sql = $this->db->query("select firmname from ndistributor where nd_code = '$nd_code'");
                    $nd_array = $sql->result_array();
                     echo $nd_array[0]['firmname'];
                    ?></td>
								    <td><?php echo ($row['item_status'] == '0' ? '<button type="submit" class="btn-sm btn-danger">Unsold</button>' : '<button type="submit" class="btn-sm btn-success">Sold</button>');?></td>
										<td><?php echo date("d-M-Y", strtotime($row['upload_date']));?></td>
									
									 	<td>
											<a style="display:inline-block;color:#000;" title="Edit" href="<?php  echo site_url(); ?>/Adminsales/edit_sale_item?id=<?php  echo base64_encode($row['stnd_id']); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											 <a  class="display:inline-block;color:#000;" title="Delete" href="<?php  echo site_url(); ?>/Adminsales/delete_sale_item?id=<?php  echo $row['imei']; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a> 
										</td> 
									</tr>
						<?php $i++; } } ?>                    
                    </tbody>
                    <tfoot>
                      <tr>
                          <td colspan="9" class="text-left">
                            <span class="pagination"><?php echo $links; ?></span>
                          </td>                        
                      </tr>
                    </tfoot>                     
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/common/listing.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

</body>
</html>
