<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	public function dashboard()
	{		
		$select	 = array('*');
		$where = array();
		$mob ='';
		if($this->session->userdata('mob')){
			$this->session->userdata('mob');
		}
	
	  //Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Admin/dashboard";
		$config["total_rows"] = $this->base_models->get_count('id','reg_user', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination('reg_user', $where,'id',$config["per_page"], $page);     
		//Pagination End

		$mob = (@$mob) ? $mob : '';
		$pagedata['select']=array('mob'=>$mob);
		$this->renderView('Admin/register_list',$pagedata);
	}
	
	public function register_list_sess()
	{
		$select	 = array('*');
		$where = array();
		$mob ='';
		//Filter Process	
		if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls')
		{
			/*$ranges = explode('-',$this->input->post('daterange'));
			$fdate = date('Y-m-d', strtotime($ranges[0])).' 00:00:00';
			$todate = date('Y-m-d', strtotime($ranges[1])).' 23:59:00';*/
			$fdate = (@$this->input->post('fromdate')) ? $this->input->post('fromdate').' 00:00:00' : '';
			$todate = (@$this->input->post('todate')) ? $this->input->post('todate').' 23:59:00' : '';
			$mob_no = (@$this->input->post('mob')) ? $this->input->post('mob') : '';
			$mob = trim($mob_no);
			$where = array();
			if($this->input->post('fromdate') != '' && $this->input->post('todate') != ''){
				if($this->session->userdata('fromdate') != NULL){
					$fdate = (@$this->session->userdata('fromdate')) ? $this->session->userdata('fromdate') : '';
					$todate = (@$this->session->userdata('todate')) ? $this->session->userdata('todate') : '';
					$where = "created_at BETWEEN '$fdate' AND '$todate'";
				}else{
					$where = "created_at BETWEEN '$fdate' AND '$todate'";
					$this->session->set_userdata(array("fromdate"=>$fdate,"todate"=>$todate));
				}
			}

			$array_items = $this->session->set_userdata(array("mob"=>$mob));
			if($this->session->userdata('mob') != NULL){
				$mob = $this->session->userdata('mob'); 
				$filter =  array('number'=> $mob);
				$where = array_merge($where,$filter);
			} elseif ($mob !=''){
				$filter =  array('number'=> $mob);
				$where = @array_merge($where,$filter);	
			}
		 
		}else{
			if($this->session->userdata('fromdate') != NULL){
				$fdate = (@$this->session->userdata('fromdate')) ? $this->session->userdata('fromdate') : '';
				$todate = (@$this->session->userdata('todate')) ? $this->session->userdata('todate') : '';
				$where = "created_at BETWEEN '$fdate' AND '$todate'";
				
			}
			if($this->session->userdata('mob') != NULL){
				$mob = $this->session->userdata('mob'); 
				$filter =  array('number'=> $mob);
				$where .= "AND number = '$mob'";
			}
		}
		
		if(@$_POST['submit']=='createxls') {
			$data['data'] = $this->base_models->GetAllValues('reg_user', $where, $select, true);
			$this->generate_reg_excel($data['data']);
		}
		//End Filter Process
	
	  //Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Admin/register_list_sess";
		$config["total_rows"] = $this->base_models->get_count('id','reg_user', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination('reg_user', $where,'id',$config["per_page"], $page);     
		//Pagination End

		$mob = (@$mob) ? $mob : '';
		$pagedata['select']=array('mob'=>$mob);  
		$this->renderView('Admin/register_list',$pagedata);
	}
	
	//generate to excel	
	public function generate_reg_excel($param1){
		// create file name
		$fileName = 'RegisterList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Mob No.');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Ward No.');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Address');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Date');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['number']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['ward']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['address']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, date('d-M-y', strtotime($element['created_at'])));
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
	public function send_sms()
	{		
		$select = array('id','name','number');
		$where = array();
		$pagedata['data'] = $this->base_models->GetAllValues('reg_user', $where, $select, true);
		$this->renderView('Admin/Sms/send_sms',$pagedata);
	}
	
	public function push_sms()
	{		
		if($_POST['users'][0] == 0) { // if selected all then fetch all users for db
			$select = array('number');			
			$nums = array_values(array_unique(array_column($this->base_models->get_records('reg_user',$select,FALSE),'number')));
		}else{
			$nums = array_values(array_unique($_POST['users']));			
		}
		
		$subj = trim($this->input->post('subject'));
		$message = trim($this->input->post('message'));
		$msg = "$subj, $message";
		$this->message_send_bulk($msg, $nums);
		
		$numsStr = implode(',',$nums);
		$insert_array=array(
						'number'=>$numsStr,
						'message'=>$msg
						);
		if($this->base_models->add_records('sms_sent',$insert_array)){
			$this->session->set_flashdata('success','SMS Sent successfully');
		}else{
			$this->session->set_flashdata('error','SMS Not Send');
		}
		redirect(base_url('Admin/send_sms'));
	}
	
	public function send_email()
	{		
		// $select = array('id','name','number');
		// $where = array();
		// $pagedata['data'] = $this->base_models->GetAllValues('reg_user', $where, $select, true);
		// $this->renderView('Admin/Sms/send_sms',$pagedata);
		echo $this->push_email();
		die();
	}
		
}
