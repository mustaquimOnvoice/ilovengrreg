<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class survey extends Base_Controller {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		if(!empty($_POST['user_type']) && $_POST['user_type'] == 'district_admin' ){
			$this->api_model->check_token('mpyc_district_admin',$_POST['user_mobile_no'],$_POST['device_token']);
		}else if(!empty($_POST['user_type']) && $_POST['user_type'] == 'user' ){
			$this->api_model->check_token('mpyc_users',$_POST['user_mobile_no'],$_POST['device_token']);
		}else{
			$response ['message'] = "fail";
			$response ['result'] =  "User type not found";
			echo json_encode($response);
			die();
		}
		
    }
	
    function index(){

        echo "call";
    }
	
    function add_survey(){
		$response ['message'] = "fail";
		$response ['result']="";
		if( isset($_POST['device_token']) && 
			isset($_POST['district'])  && 
			isset($_POST['assembly'])  && 
			isset($_POST['location'])  && 
			isset($_POST['date']) && 
			isset($_POST['user_mobile_no'])         
		){
			$TableValues['district']=$_POST['district'];
			$TableValues['assembly']=$_POST['assembly'];
			$TableValues['location']=$_POST['location'];
			$TableValues['date']=$_POST['date'];
			$TableValues['user_mobile_no']=$_POST['user_mobile_no'];
			$TableValues['device_token']=$_POST['device_token'];

			$id= $this->Base_Models->AddValues ( "mpyc_survey", $TableValues );
			foreach ($_FILES as $key => $value) {
				$imgresponse = $this->uploadImageFile($value,$id );            
			}
			
			////get end user details
			// $usercondition = array('user_mobile_no'=>$_POST['user_mobile_no']);
			// $user_details = $this->Base_Models->GetAllValues('mpyc_users', $usercondition, array('user_district'));
			// $user_district = $user_details[0]['user_district'];
			
			////search districtadmin from district
			// $admindetails = $this->Base_Models->CustomeQuary("SELECT ad.user_id as admin_id, ad.device_token as device_token, ad.user_mobile_no FROM mpyc_districts as d LEFT JOIN mpyc_district_admin as ad ON ad.user_id = d.districtAdminId WHERE d.districtName = '$user_district'");
			
			// $message = "Event: ".$_POST['title']." has been hosted ";
				// $cnt=count($admindetails);
				// for($x=0;$x<$cnt;$x++){
					////send firebase notification
					// if(!empty($admindetails[$x]['device_token'])){
						// $this->pushNotification($admindetails[$x]['device_token'], $message, $admindetails[$x]['admin_id'], '1', "Event", $id);
					// }
					
					////send sms to mobile
					////$this->message_send ( $message,  $admindetails[$x]['user_mobile_no']);
				// }

			$response ['message'] = "done";
			$response ['result']="Survey added successfully";
		}

		// log_message('error', 'img  file: '.print_r($_POST,true));
		   echo json_encode($response);
    }
	
	function uploadImageFile($file,$user_id,$type=5) {
        $response ['message'] = "fail";
        if (isset ( $user_id) ) {
            if (isset ( $file ) && $file ['error'] == 0) {
                if (! file_exists ( APPPATH . "../uploads/" . $user_id )) {
                    mkdir ( APPPATH . "../uploads/" . $user_id, 0777, true );
                }
                    log_message('error', 'img  file: '.print_r($file,true));
                $temp = "uploads/" . $user_id . "/images_unitglo_mobile-" . $this->generate_random_string ( 10 );
                if ($temp != "") {
                    $image_folder = APPPATH . "../" . $temp;
                    list ( $a, $b ) = explode ( '.', $file ['name'] );
                    $result = $this->imageCompress ( $file ['tmp_name'], $image_folder . "." . $b, 80 );
                    if ($result != '') {
                        $response ['message'] = "done";
                        $response ['image_url'] = base_url ( $temp . "." . $b );

                        $TableValues ['ref_id'] = $user_id;
                        $TableValues ['type'] = $type;
                        $TableValues ['image_url'] = $response ['image_url'];

                        $response ['upload_id'] = $this->Base_Models->AddValues ( "images", $TableValues );
                    }
                }
            }
        }
        //log_message('error', 'img : '.print_r($response,true));
        return  $response ;
    }

	function survey_list(){
	   $response ['message'] = "fail";
		$response ['result'] =  "Unable to access";
		$event_list=null;

		if(isset($_POST['user_mobile_no']) && isset($_POST['device_token'])){
			
		// $this->api_model->check_token('mpyc_district_admin',$_POST['user_mobile_no'],$_POST['device_token']);
		
		  if(isset($_POST['survey_id'])){
				$event_list= $this->Base_Models->GetAllValues ( "mpyc_survey" ,array('id' => $_POST['survey_id'] ));
				foreach ($event_list as $key => $value) {
					$event_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"5")  );
					$event_list[$key]["images"]=json_encode($event_images);
				}
				$response ['message'] = "done";
				$response ['result'] =  "Survey List";
		  }else{
				// $sTerms = explode(',', $_POST['user_district']);
				// $sTermBits = array();
				// foreach ($sTerms as $term1) {
					// $term1 = trim($term1);
					// if (!empty($term1)) {
						// $sTermBits[] = "user_district = '$term1'";
					// }
				// }
				// $mobile_numbs = $this->Base_Models->CustomeQuary("SELECT user_mobile_no FROM mpyc_users WHERE ".implode(' OR ', $sTermBits));
								
				//// $searchTerms = explode(',', $_POST['user_district']);
				// $searchTerms = $mobile_numbs;
				// $searchTermBits = array();
				// foreach ($searchTerms as $term) {
					// $term = trim($term['user_mobile_no']);
					// if (!empty($term)) {
						// $searchTermBits[] = "user_mobile_no = $term";
					// }
				// }
				
				//pagination
					// $r = $this->Base_Models->CustomeQuary("SELECT COUNT('id') as cnt FROM mpyc_survey WHERE ".implode(' OR ', $searchTermBits));
					$r = $this->Base_Models->CustomeQuary("SELECT COUNT('id') as cnt FROM mpyc_survey");
	
					$numrows = $r[0]['cnt'];
					// number of rows to show per page
					$rowsperpage = 10;
					 
					// find out total pages
					$totalpages = ceil($numrows / $rowsperpage);
					 
					// get the current page or set a default
					if (isset($_POST['currentpage']) && is_numeric($_POST['currentpage'])) {
						$currentpage = (int) $_POST['currentpage'];
					} else {
						$currentpage = 1;  // default page number
					}
					 
					// if current page is less than first page
					if ($currentpage < 1) {
						// set current page to first page
						$currentpage = 1;
					}
					 
					// the offset of the list, based on current page
					$offset = ($currentpage - 1) * $rowsperpage;
				//pagination end
				
					// if current page is greater than total pages
					if ($currentpage > $totalpages) {
						// set current page to last page
						// $currentpage = $totalpages;
						$event_list = array();
					}else{
						// $event_list = $this->Base_Models->CustomeQuary("SELECT * FROM mpyc_survey WHERE ".implode(' OR ', $searchTermBits). " ORDER BY id DESC LIMIT $offset, $rowsperpage");
						$event_list = $this->Base_Models->CustomeQuary("SELECT * FROM mpyc_survey ORDER BY id DESC LIMIT $offset, $rowsperpage");
										
						foreach ($event_list as $key => $value) {
								$event_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"5")  );
								$event_list[$key]["images"]=json_encode($event_images);
							}
					}
				$response ['message'] = "done";
				$response ['result'] =  "Survey List";

		  }
				$response ['events'] =  $event_list;
		}

		echo json_encode($response);
	}
	
}
?>