<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class atsm extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		if(!empty($_POST['atsm_code']) && !empty($_POST['device_token'])){
			$this->api_model->check_token('atsm',$_POST['atsm_code'],$_POST['device_token']);
		}else{
			$response ['message'] = "fail";
			$response ['result'] =  "Param not found";
			echo json_encode($response);
			die();
		}		
    }

    function index(){
        echo "call";
    }
	
	function uploadImageFile($file,$user_id,$type=0) {
        $response ['message'] = "fail";
        if (isset ( $user_id) ) {
            if (isset ( $file ) && $file ['error'] == 0) {
                if (! file_exists ( APPPATH . "../uploads/" . $user_id )) {
                    mkdir ( APPPATH . "../uploads/" . $user_id, 0777, true );
                }
                    log_message('error', 'img  file: '.print_r($file,true));
                $temp = "uploads/" . $user_id . "/images_unitglo_mobile-" . $this->generate_random_string ( 10 );
                if ($temp != "") {
                    $image_folder = APPPATH . "../" . $temp;
                    list ( $a, $b ) = explode ( '.', $file ['name'] );
                    $result = $this->imageCompress ( $file ['tmp_name'], $image_folder . "." . $b, 80 );
                    if ($result != '') {
                        $response ['message'] = "done";
                        $response ['image_url'] = base_url ( $temp . "." . $b );

                        $TableValues ['ref_code'] = $user_id;
                        $TableValues ['type'] = $type;
                        $TableValues ['image_url'] = $response ['image_url'];

                        $response ['upload_id'] = $this->Base_Models->AddValues ( "images", $TableValues );
                    }
                }
            }
        }
        //log_message('error', 'img : '.print_r($response,true));
        return  $response ;
    }
	
	//update profile ATSM
	function profile(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		
		if(isset($_POST['device_token']) && isset($_POST['atsm_id']) && isset($_POST['fname']) && isset($_POST['lname'])){
			
			$select = array('atsm_id','atsm_code','fname','lname','lname','email','contact','address','(select image_url from images where type = "0" AND ref_code = atsm.atsm_id ORDER BY id DESC limit 1) as image');
			$details = $this->Base_Models->GetAllValues ( "atsm", array ("atsm_id" => $_POST['atsm_id']),$select );
			if(count($details)==1){	
				$tbdata['fname']=$_POST['fname'];
				$tbdata['lname']=$_POST['lname'];
				if(isset($_POST['email'])){
					$tbdata['email']=$_POST['email'];
				}
				if(isset($_POST['contact'])){
					$tbdata['contact']=$_POST['contact'];
				}
				if(isset($_POST['password'])){
					$tbdata['password']= md5(trim($_POST['password']));
				}

				//update info
				$temp = $this->Base_Models->UpadateValue ( "atsm",$tbdata, array ("atsm_id" => $details[0] ['atsm_id'] ) );
				$response ['message'] = "done";
				$response ['result'] = "Profile updated successfully";
				
				//upload pic
				foreach ($_FILES as $key => $value) {
					$imgresponse = $this->uploadImageFile($value,$details[0]['atsm_id'] ,"0");
				}
				
				//fetch all details after update
				$returndata = $this->Base_Models->GetAllValues ( "atsm", array ("atsm_id" => $_POST['atsm_id']),$select );
				
				//return password if isset
				if(isset($_POST['password'])){
					$returndata[0]['password']= trim($_POST['password']);
				}
				
				//if no pic set default
				if(empty($returndata[0]['image'])){
					$returndata[0]['image'] = base_url().'uploads/avatar.png';
				}
				
				$response ['data'] = $returndata[0];
				
			}else{
				$response ['result'] = "User Not exist";
			}
			// log_message('error', 'update : '.print_r($temp,true));
		}
		echo json_encode($response);
	}
	
	function tsm_attendance(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		$tsm_list=null;

		$select = array('atsm_id','atsm_code','fname','username','email','contact','pan_no');
		if(!empty($_POST['atsm_id'] && !empty($_POST['attendance_date']))){
			
			$attendance_date = date('Y-m-d',strtotime($_POST['attendance_date']));			
			$tsm_list= $this->Base_Models->GetAllValues ( "atsm" ,array('upline_id' => $_POST['atsm_id'],'level_type' => '1'),$select);
			foreach ($tsm_list as $key => $value) {
				$attendance= $this->api_model->check_tsm_attendance('atsm_attendance',$tsm_list[$key]['atsm_code'],$attendance_date);
				$tsm_list[$key]["attendance"]=$attendance;
			}
			$response ['message'] = "done";
			$response ['result'] =  "TSM List";
		}
		$response ['tsm_list'] =  $tsm_list;
		echo json_encode($response);
	}
}
?>