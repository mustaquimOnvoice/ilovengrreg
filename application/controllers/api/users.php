<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class users extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		if(!empty($_POST['user_type']) && $_POST['user_type'] == 'district_admin' ){
			$this->api_model->check_token('mpyc_district_admin',$_POST['user_mobile_no'],$_POST['device_token']);
		}else if(!empty($_POST['user_type']) && $_POST['user_type'] == 'user' ){
			$this->api_model->check_token('mpyc_users',$_POST['user_mobile_no'],$_POST['device_token']);
		}else{
			$response ['message'] = "fail";
			$response ['result'] =  "User type not found";
			echo json_encode($response);
			die();
		}
	}
	
	function profile(){
			
		$response ['message'] = "fail";
		$response ['result']="";
		if(isset($_POST['device_token']) && 
			isset($_POST['age'])  && 
			isset($_POST['fname'])  && 
			isset($_POST['lname'])  && 
			isset($_POST['emial']) && 
			isset($_POST['designation']) &&
			isset($_POST['user_address']) &&
			isset($_POST['user_district']) &&			
			isset($_POST['user_mobile_no'])&&			
			isset($_POST['user_role']) &&			
			isset($_POST['assembly']) &&			
			isset($_POST['facebook']) &&			
			isset($_POST['twitter'])		
		){
			
			$details = $this->Base_Models->GetAllValues ( "mpyc_users", array (
							"user_mobile_no" => $_POST['user_mobile_no']
					) );

			if(count($details)==1){
			
				$tbdata['age']=$_POST['age'];
				$tbdata['fname']=$_POST['fname'];
				$tbdata['lname']=$_POST['lname'];
				$tbdata['user_address']=$_POST['user_address'];
				$tbdata['user_district']=$_POST['user_district'];
				$tbdata['user_role']=$_POST['user_role'];
				$tbdata['assembly']=$_POST['assembly'];

				$tbdata['facebook']=$_POST['facebook'];
				$tbdata['twitter']=$_POST['twitter'];

				$tbdata['email']=$_POST['emial'];
				$tbdata['designation']=$_POST['designation'];
				$tbdata['user_is']="existing";
				$_POST['image_url']="";
				$response ['message'] = "done";
				foreach ($_FILES as $key => $value) {
					$imgresponse = $this->uploadImageFile($value,$details[0] ['user_id'] );
					$_POST['image_url']=$imgresponse['image_url'];
				}

					$temp = $this->Base_Models->UpadateValue ( "mpyc_users",$tbdata, array (
								"user_id" => $details[0] ['user_id'] 
						) );
				$response ['result'] = "Profile updated successfully";
				
			}else{
				$response ['result'] = "User Not exist";

			}

				log_message('error', 'update : '.print_r($temp,true));
		}else{
				$response ['result'] = "Pram Not match";
		}
			log_message('error', 'Post OBJ : '.print_r($_POST,true)." File ".print_r($_FILES,true));
			$response ['result'] = json_encode($_POST);
			log_message('error', 'response : '.print_r($response,true));
			echo json_encode ( $response );
	}
	
	function profile_details(){
			
		$response ['message'] = "fail";
		$response ['result']="";
		if(isset($_POST['device_token']) && 
			
			isset($_POST['user_mobile_no']) 		
		){
			// 		$tbdata['user_district']=$_POST['user_district'];
			// $tbdata['user_role']=$_POST['user_role'];
			// $tbdata['assembly']=$_POST['assembly'];

		$details = $this->Base_Models->GetAllValues ( "mpyc_users", array (
						"user_mobile_no" => $_POST['user_mobile_no']
				),"*, (SELECT image_url FROM  `images` where ref_id=mpyc_users.user_id ORDER BY id DESC LIMIT 1) as image_url" );

			$response ['result'] = json_encode($details);

		
			
		}
		echo json_encode ( $response );
	}
	
	function uploadImageFile($file,$user_id,$type=0) {
		$response ['message'] = "fail";
		if (isset ( $user_id) ) {
			if (isset ( $file ) && $file ['error'] == 0) {
				if (! file_exists ( APPPATH . "../uploads/" . $user_id )) {
					mkdir ( APPPATH . "../uploads/" . $user_id, 0777, true );
				}
					log_message('error', 'img  file: '.print_r($file,true));
				$temp = "uploads/" . $user_id . "/images_unitglo_mobile-" . $this->generate_random_string ( 10 );
				if ($temp != "") {
					$image_folder = APPPATH . "../" . $temp;
					list ( $a, $b ) = explode ( '.', $file ['name'] );
					$result = $this->imageCompress ( $file ['tmp_name'], $image_folder . "." . $b, 80 );
					if ($result != '') {
						$response ['message'] = "done";
						$response ['image_url'] = base_url ( $temp . "." . $b );

						$TableValues ['ref_id'] = $user_id;
						$TableValues ['type'] = $type;
						$TableValues ['image_url'] = $response ['image_url'];

						$response ['upload_id'] = $this->Base_Models->AddValues ( "images", $TableValues );
					}
				}
			}
		}
		//log_message('error', 'img : '.print_r($response,true));
		return  $response ;
	}

	function update_profile(){
		$response ['message'] = "fail";
		if(isset($_POST['device_token']) && isset($_POST['age'])  && isset($_POST['fname'])  && isset($_POST['lname'])  && isset($_POST['emial']) ){

		}	

		echo json_encode ( $response );
	}
	
	function notification(){

	   $response ['message'] = "fail";
		$response ['result'] =  "Unable to access";
		if(isset($_POST['user_mobile_no']) && isset($_POST['device_token'])){
	  
			$user_notification = $this->Base_Models->GetAllValues ( "user_notification" ,array("device_token"=>$_POST['device_token']) );

				
			$response ['message'] = "done";
			$response ['result'] =  "Notification List";
			$response ['notifications'] =  $user_notification;
		}

		echo json_encode($response);
	}
	
	function contact_us(){
		$response ['message'] = "fail";
		$response ['result']="";
		if( isset($_POST['device_token']) &&
			isset($_POST['desciption'])  && 
			isset($_POST['user_mobile_no'])         
		){
			$TableValues['desciption']=$_POST['desciption'];
			$TableValues['user_mobile_no']=$_POST['user_mobile_no'];
			$TableValues['device_token']=$_POST['device_token'];

			$id= $this->Base_Models->AddValues ( "mpyc_contact", $TableValues );
			foreach ($_FILES as $key => $value) {
				$imgresponse = $this->uploadImageFile($value,$id,4);            
			}
			$response ['message'] = "done";
			$response ['result']="Contact successfully";
		}

		// log_message('error', 'img  file: '.print_r($_POST,true));
		   echo json_encode($response);
    }
	
}
?>